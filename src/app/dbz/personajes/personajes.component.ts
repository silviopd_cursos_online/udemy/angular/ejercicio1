import {Component, Input, OnInit} from '@angular/core';
import {Personaje} from "../interface/personaje";
import {DbzService} from "../service/dbz.service";

@Component({
  selector: 'app-personajes',
  templateUrl: './personajes.component.html',
  styleUrls: ['./personajes.component.css']
})
export class PersonajesComponent implements OnInit {

  get personajes(): Personaje[] {
    return this.dbzService.personajes;
  }

  constructor(private dbzService: DbzService) {
  }

  ngOnInit(): void {
  }

}
