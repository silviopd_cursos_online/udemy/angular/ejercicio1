import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Personaje} from "../interface/personaje";
import {DbzService} from "../service/dbz.service";

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.component.html',
  styleUrls: ['./agregar.component.css']
})
export class AgregarComponent implements OnInit {


  @Input() nuevo: Personaje = {
    nombre: '',
    poder: 0
  }

  // @Output() onAgregarPersonaje: EventEmitter<Personaje> = new EventEmitter();

  constructor(private dbzService: DbzService) {
  }


  ngOnInit(): void {
  }

  agregar = () => {
    if (this.nuevo.nombre.trim().length === 0) {
      return
    }

    // this.onAgregarPersonaje.emit(this.nuevo);
    this.dbzService.agregarPersonaje(this.nuevo);
    console.log(this.nuevo);

    this.nuevo = {
      nombre: '',
      poder: 0
    }
  }
}
