import {Injectable} from '@angular/core';
import {Personaje} from "../interface/personaje";

@Injectable({
  providedIn: 'root'
})
export class DbzService {

  private _personajes: Personaje[] = [
    {
      nombre: 'Goku',
      poder: 9000
    }, {
      nombre: 'Vegeta',
      poder: 8000
    }
  ]

  get personajes(): Personaje[] {
    return [...this._personajes];
  }

  constructor() {
    console.log('DbzService constructor');
  }

  agregarPersonaje(personaje: Personaje) {
    this._personajes.push(personaje);
  }
}
