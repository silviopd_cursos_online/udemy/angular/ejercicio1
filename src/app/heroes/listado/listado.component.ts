import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styleUrls: ['./listado.component.css']
})
export class ListadoComponent implements OnInit {

  heroes: string[] = ['Aquaman', 'Superman', 'Batman', 'Ironman'];
  heroesBorrados: string[] = [];

  constructor() { }

  ngOnInit(): void {
  }

  deleteHeroe = () => {
    let hb = this.heroes.pop()
    hb ? this.heroesBorrados.push(hb) : null
  }

  deleteHeroeBorrados = () => {
    this.heroesBorrados.pop()
  }

}
