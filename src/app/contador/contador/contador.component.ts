import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contador',
  templateUrl: './contador.component.html',
  styleUrls: ['./contador.component.css']
})
export class ContadorComponent implements OnInit {

  title: string = 'Contador App';

  base: number = 5;
  contador: number = 0;

  constructor() { }

  ngOnInit(): void {
  }

  acumulador(valor: number) {
    this.contador += valor;
  }
}
